﻿using System;
using System.IO;

namespace Day_4
{
    class Program
    {
        static void Main(string[] args)
        {
            SplitSpace(Console.ReadLine());
        }

        public static void SplitSpace(string kalimat)
        {

            string[] result = kalimat.Split(" ");

            foreach (string res in result)
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + res + ".txt");
            }
        }
    }
}
